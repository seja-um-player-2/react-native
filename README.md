
# Desafio FrontEnd - React Native - Player 2

Olá, obrigado por participar de nosso processo seletivo, sabemos que é bastante difícil encontrar tempo, principalmente quando se está trabalhando, por isso, nosso muito obrigado pelo seu tempo e dedicação.

O código produzido deve estar versionado em algum repositório público (Github, Bitbucket etc.).

Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio React Native- <%SEU_NOME%> " e o link para o seu repositório.

## Conhecimentos necessários para a vaga

- [ ] ReactJS
- [ ] HTML
- [ ] CSS (SASS, Styled Components, StyleSheet...)
- [ ] Gerenciamento de Estado (Redux, MobX...)
- [ ] Consumir APIs externas
- [ ] Responsividade
- [ ] Autenticação
- [ ] Build para Android e iOS
- [ ] Deploy para as lojas de aplicativos
- [ ] Otimização
- [ ] React Native cli

Diferencial:
- [ ] Desenvolvimento Android e iOS
- [ ] NoSQL
- [ ] Typescript
- [ ] Monitoramento do app
- [ ] Debug remoto
- [ ] Persistência de dados

## Por onde começar
Esse desafio tem como intuito que você mostre seus conhecimentos em criar um app usando o React Native que tenha uma boa experiência de usuário e se integre com os serviços necessários.

## Requisitos
O designer projetou a UI e está no Figma, você deve criar um projeto em React Native a partir desse layout.

- [ ] O app deve ter rotas privadas, onde somente após o login é possível acessar.
- [ ] O login pode ser simulado, ou seja, não é necessário se autenticar em uma API real. Porém, é necessário gerar um token para simular as rotas privadas e públicas.
- [ ] Utilizar Redux(ou semelhante) para persistir dados.
- [ ] Os produtos virão do seguinte endpoint: [GET] https://challenge.player2.tech/react-native/api/products 
- [ ] A Busca deve ser local, no app.
- [ ] Não é necessário enviar o pedido para algum endpoint.
- [ ] Ao final, você deve gerar um Apk.

## Link do Design
[Link do Figma](https://www.figma.com/file/7WtQ5tJQCXPM7FBu67n450/Desafio-React-Native)
Faça login(ou crie) com sua conta do Figma que você poderá inspecionar os elementos.

Você não precisa subir o sistema online, porém subir em algum serviço em nuvem é interessante.

## Final
Quando estiver tudo pronto, por favor, envie um e-mail para sejaum@player2.tech com o assunto "[Player 2] - Desafio React Native - <%SEU_NOME%> " e o link para o seu repositório e agendarmos uma call para você apresentar.

## Dúvidas
Mande um whatsapp para:

(11) 9-3379-6168 - Fábio

(71) 9-9979-6332 - André Batista

(75) 9-9259-5185 - Joabe Fernandes
